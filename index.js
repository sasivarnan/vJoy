var ref = require('ref');
var ffi = require('ffi');
var Struct = require('ref-struct');
var os = require('os');

var vJoyInterface = './SDK/lib/' + (os.arch() == 'x64' ? 'amd64/' : '') + 'vJoyInterface';

var deviceId = 1;

var vJDStats = {
  VJD_STAT_OWN: 0,
  VJD_STAT_FREE: 1,
  VJD_STAT_BUSY: 2,
  VJD_STAT_MISS: 3,
  VJD_STAT_UNKN: 4
};


var dword = ref.types.uint32;
var word = ref.types.uint16;
var voidType = ref.types.void;
var voidPtr = ref.refType(voidType);

var JoystickPosition = Struct({
  bDevice: 'byte',
  wThrottle: 'long',
  wRudder: 'long',
  wAileron: 'long',
  wAxisX: 'long',
  wAxisY: 'long',
  wAxisZ: 'long',
  wAxisXRot: 'long',
  wAxisYRot: 'long',
  wAxisZRot: 'long',
  wSlider: 'long',
  wDial: 'long',
  wWheel: 'long',
  wAxisVX: 'long',
  wAxisVY: 'long',
  wAxisVZ: 'long',
  wAxisVBRX: 'long',
  wAxisVBRY: 'long',
  wAxisVBRZ: 'long',
  lButtons: 'long',	// 32 buttons : 'long', 0x00000001 means button1 is pressed, 0x80000000 -> button32 is pressed
  bHats: word,	// Lower 4 bits: HAT switch or 16-bit of continuous HAT switch
  bHatsEx1: word,	// Lower 4 bits: HAT switch or 16-bit of continuous HAT switch
  bHatsEx2: word,	// Lower 4 bits: HAT switch or 16-bit of continuous HAT switch
  bHatsEx3: word,	// Lower 4 bits: HAT switch or 16-bit of continuous HAT switch long lButtonsEx1 : // Buttons 33-64

  /// JOYSTICK_POSITION_V2 Extenssion
  lButtonsEx1: 'long', // Buttons 33-64
  lButtonsEx2: 'long',// Buttons 65-96
  lButtonsEx3: 'long'// Buttons 97-128
});

var joyStick = ffi.Library(vJoyInterface, {
  'DriverMatch': ['bool', [dword, dword]],
  'vJoyEnabled': ['bool', []],
  'GetvJoyVersion': ['short', []],
  'GetvJoyProductString': ['short', []],
  'GetvJoySerialNumberString': ['short', []],
  'GetVJDStatus': ['int', ['int']],
  'AcquireVJD': ['bool', ['int']],
  'FfbStart': ['bool', ['int']],
  'FfbRegisterGenCB': [voidType, [voidPtr, voidPtr]],
  'UpdateVJD': ['bool', ['int', voidPtr]],
  'RelinquishVJD': [voidType, ['int']],
  'ResetVJD': ['bool', ['int']]
});

const DRIVER_VER = '2.1.6',
  DLL_VER = '2.1.6';


//check if vJoy is installed and enabled
if (joyStick.vJoyEnabled()) {
  console.log("JoyStick installed");
  console.log("Product String", joyStick.GetvJoyProductString());
  console.log("Serial Number", joyStick.GetvJoySerialNumberString());
  console.log("Version", joyStick.GetvJoyVersion());
} else {
  console.log("vJoy joyStick not installed or enabled");
}

//check if vJoy driver version and interfacedll version are same
if (joyStick.DriverMatch(DLL_VER, DRIVER_VER)) {
  console.log('Driver - Yes');
} else {
  console.log('Driver - No');
}


//get and check status of vJoy device
var vJDStat = joyStick.GetVJDStatus(deviceId);

switch (vJDStat) {
  case vJDStats.VJD_STAT_OWN:
    console.log("vJoy device %d is already owned by this feeder.", deviceId);
    break;
  case vJDStats.VJD_STAT_FREE:
    console.log("vJoy device %d is free.", deviceId);
    break;
  case vJDStats.VJD_STAT_BUSY:
    console.log("vJoy device %d is already owned by another feeder. Cannot continue.", deviceId);
    break;
  case vJDStats.VJD_STAT_MISS:
    console.log("vJoy device %d is not installed or disabled. Cannot continue.", deviceId);
    break;
  default:
    console.log("vJoy device %d general error. Cannot continue.", deviceId);
    break;
};


//Acquire the joystick
if (joyStick.AcquireVJD(deviceId)) {
  console.log('Joystick %d acquired', deviceId);
} else {
  console.log('Failed to acquire joystick', deviceId);
}

//Start FFB
if (joyStick.FfbStart(deviceId)) {
  console.log("FFB Enabled")
} else {
  console.log("Falied to enable FFB");
}

var feedbackFunction = ffi.Callback(voidType, [voidPtr, voidPtr], function (a, b) {
  console.log(a, b);
});


//register feedback function
joyStick.FfbRegisterGenCB(feedbackFunction, null);

var iReport = new JoystickPosition();

var X = 0;
var Y = 0;
var Z = 0;
var Btns = 0;

var i = 1, j = 32;
//process.exit(0);

btn = '11';

// Start endless loop
// The loop injects position data to the vJoy device
// If it fails it let's the user try again
var updateJoystickStatus = setInterval(function () {
  iReport.bDevice = deviceId;
  Z > 35000 && (Z = 0);

  // Set position data of 3 first axes
  Z += 200;
  //  iReport.wAxisZ = Z;
  //iReport.wAxisX = 32000 - Z;
  //iReport.wAxisY = Z / 2 + 7000;

  // iReport.wAxisVZ = Z;
  // iReport.wAxisVX = 32000 - Z;
  // iReport.wAxisVY = Z / 2 + 7000;

  // if (i < 0) {
  //   iReport.wAxisZ = i;
  //   iReport.wAxisX = i;
  //   iReport.wAxisY = i;
  // } else {
  //   iReport.wAxisVZ = i;
  //   iReport.wAxisVX = i;
  //   iReport.wAxisVY = i;
  // }

  // i *= -1;

  // i > 32768 && (i=0)
  // // iReport.wAxisZ = i;
  // // iReport.wAxisX = i;
  // // iReport.wAxisY = i;

  iReport.wAxisZRot = 0;
  iReport.wAxisXRot = i;
  iReport.wAxisYRot = i;

  i > 32768 && (i = 0)
  i += 200;
  iReport.wAxisZ = 0;
  iReport.wAxisX = i;
  iReport.wAxisY = 0;

  iReport.wDial = i;
  iReport.wSlider = i;


  // Set position data of first 8 buttons
  //Btns = 1 << (Z / 4000);
  j == 1 && (j = 32)
  btn += '1';
  Btns = parseInt(btn, 2);
  //console.log(Btns);
  iReport.lButtons = Btns;

  iReport.bHats = 0x1;

  if (joyStick.UpdateVJD(deviceId, iReport.ref())) {
    console.log("Joystick position Updated")
  } else {
    console.log("Joystick position not Updated");
    joystick.AcquireVJD(deviceId);
  }

}, 500);

//console.log(iReport.toJSON());


//Actions to be taken when the application exit
function exitHandler(code) {
  clearInterval(updateJoystickStatus);
  joyStick.ResetVJD(deviceId);
  joyStick.RelinquishVJD(deviceId);
  console.log(code);
}

//do something when app is closing
process.on('exit', exitHandler);

//catches ctrl+c event
process.on('SIGINT', exitHandler);

//catches uncaught exceptions
process.on('uncaughtException', exitHandler);